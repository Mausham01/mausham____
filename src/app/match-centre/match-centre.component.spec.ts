import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchCentreComponent } from './match-centre.component';

describe('MatchCentreComponent', () => {
  let component: MatchCentreComponent;
  let fixture: ComponentFixture<MatchCentreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatchCentreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchCentreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
