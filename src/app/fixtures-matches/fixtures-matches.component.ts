import { Component, OnInit } from '@angular/core';
import {Observable} from  'rxjs';
import { GameServiceService } from '../gameService/game-service.service';
import { Game } from '../models/game';

@Component({
  selector: 'app-fixtures-matches',
  templateUrl: './fixtures-matches.component.html',
  styleUrls: ['./fixtures-matches.component.css']
})
export class FixturesMatchesComponent implements OnInit {

  games: Game[];

  constructor(private gameService:GameServiceService) { }

  ngOnInit() {
    this.getAFLGames();
  }

  getAFLGames():void{
    this.gameService.getGames().subscribe(
      temp=>{
        this.games=temp;}
      
    )
  }

}
