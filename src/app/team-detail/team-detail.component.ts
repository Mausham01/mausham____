import { Component, OnInit,OnDestroy } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { DataServiceService } from '../dataService/data-service.service';
import { Team } from '../models/team';
@Component
({
  selector: 'app-team-detail',
  templateUrl: './team-detail.component.html',
  styleUrls: ['./team-detail.component.css']
})
export class TeamDetailComponent implements OnInit , OnDestroy
{
  teams: Team[];
  private routeSub:any;
  id:number;
  name:string;

  constructor(private route:ActivatedRoute,
    private  dataService: DataServiceService) { }

  ngOnInit() {
    this.getAFLTeams();
    this.routeSub=this.route.params.subscribe(params=>{

      this.id=params['id']
      
      
    })
  }
  getAFLTeams():void{
    this.dataService.getTeams().subscribe(temp=>{this.teams=temp;});
  }

  ngOnDestroy()
  {
    this.routeSub.unsubscribe()

  }

}
