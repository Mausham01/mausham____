import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import { Team} from '.././models/team';

@Injectable
({
  providedIn: 'root'
})

export class DataServiceService {

  constructor(private http: HttpClient) { }

  ngOnInit()
  {
    this.getTeams();
  }

  getTeams() : Observable<Team[]>
  {
    return this.http.get('https://api.squiggle.com.au/?q=teams').pipe(
      map((data:any)=>data.teams.map((item:any)=>new Team(
        item.logo,
        item.name,
        item.abbrev,
        item.id
      ))
    ))
  }


}
