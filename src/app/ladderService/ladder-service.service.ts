import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import { Ladder } from '../models/ladder';


@Injectable({
  providedIn: 'root'
})
export class LadderServiceService {

  constructor(private http:HttpClient) { }

  ngOnInit()
  {
    this.getLadder();
  }

  getLadder():Observable<Ladder[]>
  {
    return this.http.get('https://api.squiggle.com.au/?q=ladder').pipe(map((data:any)=>data.ladder.map((item3:any)=>new Ladder(
      item3.round,
      item3.team,
      item3.year,
      item3.teamid,
      item3.sourceid,
      item3.updated,
      item3.wins,
      item3.mean_rank,
      item3.rank,
      item3.source,
      item3.percentage,
      item3.swarms


    ))))
  }
}