import { TestBed } from '@angular/core/testing';

import { SourcesServiceService } from './sources-service.service';

describe('SourcesServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SourcesServiceService = TestBed.get(SourcesServiceService);
    expect(service).toBeTruthy();
  });
});
