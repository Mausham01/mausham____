import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import { Source } from '.././models/sources';

@Injectable({
  providedIn: 'root'
})
export class SourcesServiceService {

  constructor(private http:HttpClient) { }

  ngOnInit()
  {
    this.getSources();
  }

  getSources():Observable<Source[]>
  {
    return this.http.get('https://api.squiggle.com.au/?q=sources').pipe
    (map((data:any)=>data.sources.map((item4:any)=>new Source(
      item4.url,
      item4.id,
      item4.name

    ))))
  }
}
