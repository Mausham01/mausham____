import { Component, OnInit } from '@angular/core';
import {Observable } from 'rxjs';
import { LadderServiceService } from '../ladderService/ladder-service.service';
import { Ladder } from '../models/ladder';

@Component({
  selector: 'app-ladder-table',
  templateUrl: './ladder-table.component.html',
  styleUrls: ['./ladder-table.component.css']
})
export class LadderTableComponent implements OnInit {

  ladder:Ladder[]

  constructor(private ladderService: LadderServiceService ) { }

  ngOnInit() 
  {
    this.getAFLLadder();

  }

  getAFLLadder():void
  {
    this.ladderService.getLadder().subscribe
    (
      temp=>{this.ladder=temp;}
    );


  }

}
