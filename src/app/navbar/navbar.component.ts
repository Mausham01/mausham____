import { Component, OnInit } from '@angular/core';
import {Observable } from 'rxjs';
import { DataServiceService } from '../dataService/data-service.service';
import { Team } from '../models/team';
import { $ } from 'protractor';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  dropItem : string;
  teams: Team[];
  constructor(private dataService: DataServiceService) { }

  ngOnInit() {
    this.getAFLTeams();
  }

  getAFLTeams():void{
    this.dataService.getTeams().subscribe(temp=>{this.teams=temp;});
  }
}
