import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ViewLeagueTableComponent } from './view-league-table/view-league-table.component';
import { DataServiceService } from './dataService/data-service.service';
import { ViewTeamGamesComponent } from './view-team-games/view-team-games.component';
import { GameServiceService } from './gameService/game-service.service';
import { LadderTableComponent } from './ladder-table/ladder-table.component';
import { LadderServiceService } from './ladderService/ladder-service.service';
import { SourcesComponentComponent } from './sources-component/sources-component.component';
import { SourcesServiceService } from './sourcesService/sources-service.service';
import { TipsComponentComponent } from './tips-component/tips-component.component';
import { TipServiceService } from './tipService/tip-service.service';
import { NavbarComponent } from './navbar/navbar.component';
import { MatchCentreComponent } from './match-centre/match-centre.component';
import { FooterComponent } from './footer/footer.component';
import { SignupComponent } from './signup/signup.component';
import { FixturesMatchesComponent } from './fixtures-matches/fixtures-matches.component';
import { TeamDetailComponent } from './team-detail/team-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    ViewLeagueTableComponent,
    ViewTeamGamesComponent,
    LadderTableComponent,
    SourcesComponentComponent,
    TipsComponentComponent,
    NavbarComponent,
    MatchCentreComponent,
    FooterComponent,
    SignupComponent,
    FixturesMatchesComponent,
    TeamDetailComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot([
      {
        path:'signup',
        component:SignupComponent,
      },
      {
        path:'',
        component:MatchCentreComponent, 
      },
      {
        path:'fixtures_matches',
        component:FixturesMatchesComponent,
      },
      {
        path:"team/:id",
        component:TeamDetailComponent
      },
      {
        path:'team',
        component:ViewLeagueTableComponent
      }

    ])
  ],
  providers: [DataServiceService,GameServiceService,LadderServiceService,
  SourcesServiceService,TipServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
