import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTeamGamesComponent } from './view-team-games.component';

describe('ViewTeamGamesComponent', () => {
  let component: ViewTeamGamesComponent;
  let fixture: ComponentFixture<ViewTeamGamesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTeamGamesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTeamGamesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
