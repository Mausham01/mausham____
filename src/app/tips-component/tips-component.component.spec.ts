import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipsComponentComponent } from './tips-component.component';

describe('TipsComponentComponent', () => {
  let component: TipsComponentComponent;
  let fixture: ComponentFixture<TipsComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipsComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipsComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
